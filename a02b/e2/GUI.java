package a02b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private final Map<JButton,Pair<Integer,Integer>> buttons = new HashMap<>();
	private final Map<Pair<Integer,Integer>,JButton> coords = new HashMap<>();
	private final Logic logic;
	private boolean hit=false;
	
    public GUI() {
    	this.setSize(500,500);
        logic=new LogicImpl(10);
        JPanel panel = new JPanel(new GridLayout(10,10));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();            
            int x = buttons.get(bt).getX();
            int y = buttons.get(bt).getY();
            if(!hit) {
            	logic.hit1(x, y);
            	hit = true;
            	bt.setEnabled(false);
            }else {
            	if(logic.hit2(x, y)) {
            		System.exit(0);
            	}else {
            		draw(logic.drawRectangle());
            		hit = false;
            	}
            }
        };
                
        for (int i=0; i<10; i++){
            for (int j=0; j<10; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(i,j));
                this.coords.put(new Pair<>(i,j), jb);
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }

	private void draw(Iterable<Pair<Integer, Integer>> drawRectangle) {
		for(Pair<Integer,Integer> p : drawRectangle) {
			coords.get(p).setText("*");
		}
	}
        
}
