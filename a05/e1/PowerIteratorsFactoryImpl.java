package a05.e1;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

	@Override
	public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
		return new PowerIterator<Integer>() {
			
			Iterator<Integer> it = Stream.iterate(start, successive).iterator();
			private LinkedList<Integer> list=new LinkedList<Integer>();
			
			@Override
			public PowerIterator<Integer> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Optional<Integer> next() {
				list.add(it.next());
				return Optional.of(list.getLast());
			}
			
			@Override
			public List<Integer> allSoFar() {
				return list;
			}
		};
	}

	@Override
	public <X> PowerIterator<X> fromList(List<X> list) {
		return new PowerIterator<X>() {
			Iterator<X> it = list.iterator();
			private LinkedList<X> outList=new LinkedList<>();
			
			@Override
			public Optional<X> next() {
				if(!it.hasNext()) {
					return Optional.empty();
				}
				outList.add(it.next());
				return Optional.of(outList.getLast());
			}

			@Override
			public List<X> allSoFar() {
				return outList;
			}

			@Override
			public PowerIterator<X> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@Override
	public PowerIterator<Boolean> randomBooleans(int size) {
		return new PowerIterator<Boolean>() {
			Random r = new Random();
			LinkedList<Boolean> list = new LinkedList<>();
			
			@Override
			public PowerIterator<Boolean> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Optional<Boolean> next() {
				if(!(list.size()<size)) {
					return Optional.empty();
				}
				list.add(r.nextBoolean());
				return Optional.of(list.getLast());
			}
			
			@Override
			public List<Boolean> allSoFar() {
				return list;
			}
		};
	}

	

}
