package a05.e2;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private static final int SIZE = 4;
    private final Map<JButton,Pair<Integer, Integer>> buttons;
    private Logic logic;
    
    public GUI() {
    	 this.setDefaultCloseOperation(EXIT_ON_CLOSE);
         this.setSize(100*SIZE, 100*SIZE);
         
         buttons=new HashMap<>();
         logic = new LogicImpl(SIZE);
         
         JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
         this.getContentPane().add(BorderLayout.CENTER,panel);
         
         ActionListener al = (e)->{
             final JButton bt = (JButton)e.getSource();
             if(logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY())) {
            	 if(!logic.isOver()) {
            		 bt.setText(String.valueOf(logic.getResult()));
                	 bt.setEnabled(false);
            	 }else {
            		 System.exit(1);
            	 }
             }         	 
         };
                 
         for (int i=0; i<SIZE; i++){
             for (int j=0; j<SIZE; j++){
                 final JButton jb = new JButton(" ");
                 jb.addActionListener(al);
                 panel.add(jb);
                 buttons.put(jb, new Pair<>(i, j));
             }
         }
         this.setVisible(true);
    }
}
