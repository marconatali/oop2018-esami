package a05.e2;

public interface Logic {
	boolean hit (int x,int y);
	int getResult();
	boolean isOver();
}
