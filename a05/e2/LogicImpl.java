package a05.e2;

import java.util.HashSet;
import java.util.LinkedList;

public class LogicImpl implements Logic {

	private LinkedList<Pair<Integer,Integer>> grid;
	private int size;
	private int cont;

	
	public LogicImpl(int size) {
		grid = new LinkedList<>();
		this.size=size;
	}
	
	@Override
	public boolean hit(int x, int y) {
		if(this.check(x,y)) {
			return grid.add(new Pair<>(x,y));
		}
		return false;
	}

	private boolean check(int x, int y) {
		if(this.grid.isEmpty()) {
			return true;
		}
		Pair<Integer,Integer> p = grid.getLast();
		return p.getX()==x && Math.abs(p.getY()-y)==1 || p.getY()==y && Math.abs(p.getX()-x)==1;
	}
	

	@Override
	public int getResult() {
		return cont ++;
	}

	@Override
	public boolean isOver() {
		Pair<Integer,Integer> p = grid.getLast();
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(p.getX()==i && Math.abs(p.getY()-j)==1 || p.getY()==j && Math.abs(p.getX()-i)==1) {
					if(!grid.contains(new Pair<>(i,j))) {
						return false;
					}
				}
			}
		}
		return true;
	}

}
