package a03b.e2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.*;


public class GUI extends JFrame {
    
    private static final long serialVersionUID = 1L;
   
    private List<JButton> buttons = null; 
    private JCheckBox box = null; 
    private Logic logic = null;
    private static int  SIZE=5;
    
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(buttons.indexOf(bt), box.isSelected());
            drawValues();
        };
        
        JPanel panel = new JPanel(new FlowLayout());
        this.buttons = IntStream.range(0,5)
                                .mapToObj(i->new JButton(""))
                                .peek(panel::add)
                                .peek(jb->jb.addActionListener(al))
                                .collect(Collectors.toList());
        logic = new LogicImpl(SIZE);
        drawValues();
        this.box = new JCheckBox("box");
        panel.add(this.box);
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.pack();
        this.setVisible(true);
    }
    
    private void drawValues() {
        if(!logic.isOver()) {
            List<Integer> list=logic.getValues();
            for(int i=0;i<SIZE;i++) {
                buttons.get(i).setText(String.valueOf(list.get(i)));
            }
        }else {
            System.exit(0);
        }        
    }
}
