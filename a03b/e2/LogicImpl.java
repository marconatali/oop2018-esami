package a03b.e2;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LogicImpl implements Logic {

    private final List<Integer> values;
    
    public LogicImpl(int size) {
       Random r = new Random();
       this.values = new LinkedList<>(IntStream.range(0,size).map(i -> r.nextInt(10)).boxed().collect(Collectors.toList()));
    }
    
    @Override
    public void hit(int i,boolean b) {
       if(!b) {
           Collections.swap(values, i, Math.min(i+1,values.size()-1));           
       }else{
           Collections.swap(values, i, Math.max(i-1,0));             
       }  
    }

    @Override
    public boolean isOver() {
        int prev = -1;
        for (int i: this.values) {
            if (prev > i) {
                return false;
            }
            prev = i;
        }
        return true;
    }

    @Override
    public List<Integer> getValues() {
        return this.values;
    }

}
