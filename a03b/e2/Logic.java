package a03b.e2;

import java.util.List;

public interface Logic {
    void hit(int i,boolean b);
    boolean isOver();
    List <Integer> getValues();
}
