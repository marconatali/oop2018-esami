package a03b.e1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ConferenceReviewingImpl implements ConferenceReviewing {

    List<Pair<Integer,Map<Question,Integer>>> articles;
    
    public ConferenceReviewingImpl() {
       this.articles=new LinkedList();
    }
    
    @Override
    public void loadReview(int article, Map<Question, Integer> scores) {
        articles.add(new Pair<Integer, Map<Question,Integer>>(article,scores));
    }

    @Override
    public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
        Map<Question, Integer> scores = new HashMap<>();
        scores.put(Question.RELEVANCE, relevance);
        scores.put(Question.SIGNIFICANCE, significance);
        scores.put(Question.CONFIDENCE, confidence);
        scores.put(Question.FINAL, fin);
        this.loadReview(article, scores);
    }

    @Override
    public List<Integer> orderedScores(int article, Question question) {
       return articles.stream()
               .filter(e->e.getX().equals(article))
               .map(e->e.getY().get(question))
               .sorted()
               .collect(Collectors.toList());
    }

    @Override
    public double averageFinalScore(int article) {
        double nArticles=(double)articles.stream().filter(e->e.getX().equals(article)).map(e->e.getY().get(Question.FINAL)).count();
        return articles.stream()
                .filter(e->e.getX().equals(article))
                .map(e->e.getY().get(Question.FINAL))
                .reduce((x,y)->x+y).get().doubleValue()/nArticles;
    }

    @Override
    public Set<Integer> acceptedArticles() {
       return articles.stream()
               .filter(e->e.getY().get(Question.RELEVANCE) >=8 && this.averageFinalScore(e.getX())>5.0)
               .map(e->e.getX())
               .sorted()
               .collect(Collectors.toSet());
    }

    @Override
    public List<Pair<Integer, Double>> sortedAcceptedArticles() {
       return this.acceptedArticles().stream()
               .map(e->new Pair<>(e,this.averageFinalScore(e)))
               .sorted((e1,e2)->e2.getX()-e1.getX())
               .collect(Collectors.toList());
    }

    @Override
    public Map<Integer, Double> averageWeightedFinalScoreMap() {
        // TODO Auto-generated method stub
        return null;
    }
}
