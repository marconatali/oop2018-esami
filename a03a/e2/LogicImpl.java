package a03a.e2;

import java.util.HashMap;
import java.util.Map;



public class LogicImpl implements Logic {

	private Map<Pair<Integer,Integer>,Integer> grid = new HashMap<>();
	int size;
	
	public LogicImpl(int size) {
		this.size=size;
		initializeMap(size);
	}
	@Override
	public void hit(int x, int y) {		
		for(int i=Math.max(0, x-1);i<=Math.min(x+1, size-1);i++) {
			for(int j=Math.max(0, y-1);j<=Math.min(y+1, size-1);j++) {
				grid.compute(new Pair<>(i,j), (k,v)->v=v+1);
			}
		}
	}

	@Override
	public int getValue(int x, int y) {
		return grid.get(new Pair<>(x,y));
	}

	@Override
	public boolean isOver() {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(grid.get(new Pair<>(i,j)) == 0) {
					return false;
				}
			}
		}
		return true;
	}
	
	private void initializeMap(int size) {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				grid.put(new Pair<>(i, j), 0);
			}
		}
	}

}
