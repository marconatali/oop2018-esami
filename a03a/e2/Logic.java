package a03a.e2;

public interface Logic {
	void hit(int x, int y);
	int getValue(int x, int y);
	boolean isOver();
}
