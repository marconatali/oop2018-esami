package a03a.e1;

import java.util.HashMap;
import java.util.Map;
import a03a.e1.Evaluation.Question;
import a03a.e1.Evaluation.Result;

public class EvaluationBuilderImpl implements EvaluationBuilder {

	private Map<Pair<Integer,String>, Map<Question, Result>> evaluations = new HashMap<>();
	private boolean built = false;
	@Override
	public EvaluationBuilder addEvaluationByMap(String course, int student, Map<Question, Result> results) {
		if(results.size()!=3) {
			throw new IllegalArgumentException();
		}
		check(!evaluations.containsKey(new Pair<>(student,course)));
		this.evaluations.put(new Pair<>(student,course), results);
		return this;
	}

	@Override
	public EvaluationBuilder addEvaluationByResults(String course, int student, Result resOverall, Result resInterest,
			Result resClarity) {
		check(!built);
		check(!evaluations.containsKey(new Pair<>(student,course)));
		Map<Question,Result> results=new HashMap<>();
		results.put(Question.OVERALL, resOverall);
		results.put(Question.CLARITY, resClarity);
		results.put(Question.INTEREST, resInterest);
		addEvaluationByMap(course,student,results);
		return this;
	}

	@Override
	public Evaluation build() {
		check(!built);
		built=true;
		return new Evaluation() {			
			
			@Override
			public Map<Result, Long> resultsCountForStudent(int student) {
				Map<Result,Long> result=new HashMap<Evaluation.Result, Long>();
				for(Result r : Evaluation.Result.values()) {
					result.put(r, evaluations.entrySet().stream()
							.filter(e->e.getKey().getX().equals(student))
							.map(e->e.getValue().entrySet().stream()
									.filter(f->f.getValue().equals(r)).count()).reduce((x,y)->x+y).get());
				}
				return result;
				
			}
			
			@Override
			public Map<Result, Long> resultsCountForCourseAndQuestion(String course, Question questions) {
				Map<Result,Long> result=new HashMap<Evaluation.Result, Long>();
				for(Result r : Evaluation.Result.values()) {
					result.put(r, evaluations.entrySet().stream()
							.filter(e->e.getKey().getY().equals(course))
							.filter(e->e.getValue().get(questions).equals(r))
							.count());
				}
				return result;
			}
			
			@Override
			public Map<Question, Result> results(String course, int student) {
				if(evaluations.containsKey(new Pair<>(student,course))) {
					 return evaluations.get(new Pair<>(student,course));
				}
				return new HashMap<>();
				
			}
			
			@Override
			public double coursePositiveResultsRatio(String course, Question question) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
	}
	
	void check(boolean b) {
		if(!b) {
			throw new IllegalStateException();
		}
	}

}
