package a06.e1;

import java.util.HashSet;
import java.util.Set;

public class ParserFactoryImpl implements ParserFactory {
	
	private Parser ParserFactoryFromSet(Set<String> set,int numElem) {
		return new Parser() {
			int nElem=numElem;
			int cont=0;
			Set<String> tokens = new HashSet<>(set);
			@Override
			public void reset() {
				cont=0;	
			}
			
			@Override
			public boolean inputCompleted() {
				return cont == nElem;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(!inputCompleted() && this.tokens.contains(token)) {
					cont++;
					return true;
				}
				return false;
			}
		};
	}

	@Override
	public Parser one(String token) {
		return ParserFactoryFromSet(Set.of(token),1);
	}

	@Override
	public Parser many(String token, int elemCount) {
		return ParserFactoryFromSet(Set.of(token),elemCount);
	}

	@Override
	public Parser oneOf(Set<String> set) {
		return ParserFactoryFromSet(set,1);
	}

	@Override
	public Parser sequence(String token1, String token2) {
		return new Parser() {
			int nElem=2;
			int cont=0;
			String t1 = token1;
			String t2 = token2;
			@Override
			public void reset() {
				cont=0;				
			}
			
			@Override
			public boolean inputCompleted() {
				return cont == nElem;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(!inputCompleted()) {
					if(cont==0) {
						cont++;
						return t1==token1;
					}else {
						cont++;
						return t2==token2;
					}
				}
				return false;
			}
		};
	}

	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		// TODO Auto-generated method stub
		return null;
	}

}
