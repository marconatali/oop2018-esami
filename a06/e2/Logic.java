package a06.e2;

import java.util.List;

public interface Logic {
	void hit(int i);
	int getValue(int i);
}
