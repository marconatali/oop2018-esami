package a06.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.stream.*;
import javax.swing.*;

import a03a.e2.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GUI extends JFrame{
	
	private final Map<JButton,Integer> buttons;
	//private static final int SIZE=5;
	private Logic logic;
	
	public GUI(int size){
		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		buttons=new HashMap<>();
		logic = new LogicImpl(size);
	
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();			
			logic.hit(buttons.get(jb));
			jb.setEnabled(false);
			jb.setText(String.valueOf(logic.getValue(buttons.get(jb))));
		};
		
		ActionListener ar = e -> {
			logic = new LogicImpl(size);
			initializeButtons();
		};
		
		for (int i=0; i<size; i++){            
            final JButton bt = new JButton();
            bt.addActionListener(ac);
            buttons.put(bt, i);
            this.getContentPane().add(bt);
        }
		initializeButtons();
		
		final JButton reset = new JButton("Reset");
        reset.addActionListener(ar);
        this.getContentPane().add(reset);      
		
		this.setVisible(true);
	}
	
	private void initializeButtons() {
		for(JButton jb : buttons.keySet()) {
			jb.setText(String.valueOf(logic.getValue(buttons.get(jb))));
			jb.setEnabled(true);
		}
	}
	
}
