package a06.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LogicImpl implements Logic {

	private final int size;
	private List<Integer> values;
	
	public LogicImpl(int size) {
		this.size=size;
		values=new LinkedList<Integer>();
		for(int i=0; i<size; i++) {
			values.add(1);
		}
	}
	@Override
	public void hit(int i) {
		int sum=0;
		for(int x=0;x<=i;x++) {
			sum+=values.get(x);			
		}
		values.set(i,Integer.valueOf(sum));
	}

	@Override
	public int getValue(int i) {
		return this.values.get(i);
	}

}
