package a04.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class IntegerIteratorsFactoryImpl implements IntegerIteratorsFactory {

    @Override
    public SimpleIterator<Integer> empty() {
        return new SimpleIterator<Integer>() {
            
            @Override
            public Optional<Integer> next() {
                return Optional.empty();
            }
        };
    }

    @Override
    public SimpleIterator<Integer> fromList(List<Integer> list) {
        Iterator<Integer> i = list.iterator();  
        return new SimpleIterator<Integer>() {                     
            @Override
            public Optional<Integer> next() {
                if(i.hasNext()) {
                    return Optional.of(i.next());
                }
                return Optional.empty();
            }
        };
    }

    @Override
    public SimpleIterator<Integer> random(int size) {
        return new SimpleIterator<Integer>() {
            Random r = new Random();
            int cont=0;
            @Override
            public Optional<Integer> next() {
                 cont++;
                 if(cont>size) {
                     return Optional.empty();
                 }
                 return Optional.of(r.nextInt(size));
            }
        };
    }

    @Override
    public SimpleIterator<Integer> quadratic() {
        return new SimpleIterator<Integer>() {            
            int value=1;
            @Override
            public Optional<Integer> next() {              
                while(true) {
                   for(int i=0;i<=value;i++){
                        return Optional.of(value);
                    }                  
                }
            }
        };
    }

    @Override
    public SimpleIterator<Integer> recurring() {
        // TODO Auto-generated method stub
        return null;
    }

}
