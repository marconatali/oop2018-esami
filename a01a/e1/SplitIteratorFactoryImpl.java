package a01a.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;


public class SplitIteratorFactoryImpl implements SplitIteratorFactory {

	@Override
	public SplitIterator<Integer> fromRange(int start, int stop) {
		return new SplitIterator<Integer>() {
			int index=start;
			@Override
			public SplitIterator<Integer> split() {
				int oldIndex=index;
				index=((stop-index)/2);
				return fromRange(oldIndex, index);
			}
			
			@Override
			public Optional<Integer> next() {
				if(index<=stop) {
					return Optional.of(index++);
				}
				return Optional.empty();
			}
		};
	}

	@Override
	public SplitIterator<Integer> fromRangeNoSplit(int start, int stop) {
		return new SplitIterator<Integer>() {
			int index=start;
			@Override
			public SplitIterator<Integer> split() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Optional<Integer> next() {
				if(index<=stop) {
					return Optional.of(index++);
				}
				return Optional.empty();
			}
		};
	}

	@Override
	public <X> SplitIterator<X> fromList(List<X> list) {
		return new SplitIterator<X>() {
			int index=0;
			@Override
			public Optional<X> next() {
				if(index<list.size()) {
					return Optional.of(list.get(index++));
				}
				return Optional.empty();
			}

			@Override
			public SplitIterator<X> split() {
				int oldIndex=index;
				index=((list.size()-index)/2);
				return fromList(list.subList(oldIndex, index));
			}
		};
	}

	@Override
	public <X> SplitIterator<X> fromListNoSplit(List<X> list) {
		return new SplitIterator<X>() {
			int index=0;
			@Override
			public Optional<X> next() {
				if(index<list.size()) {
					return Optional.of(list.get(index++));
				}
				return Optional.empty();
			}

			@Override
			public SplitIterator<X> split() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public <X> SplitIterator<X> excludeFirst(SplitIterator<X> si) {
		si.next();
		return new SplitIterator<X>() {
			SplitIterator<X> s= si;
			@Override
			public Optional<X> next() {
				return si.next();
			}

			@Override
			public SplitIterator<X> split() {
				return si.split();
			}
		};
		
	}

	@Override
	public <X, Y> SplitIterator<Y> map(SplitIterator<X> si, Function<X, Y> mapper) {
		// TODO Auto-generated method stub
		return null;
	}

}
